#lang racket
(require
  2htdp/image
  2htdp/universe
  racket/random)
(require "effect.rkt")
(include "graphics.rkt")

;; try to use lux for advanced features
;; http://docs.racket-lang.org/lux/index.html
;; I don't need them right now

;; x y 0,0 is top left
;; x y in also center of image

; game constants
(define window-width 640)
(define window-height 480)

(define rectangle-picture-size 206)
(define princess-space-size 6)
(define num-princess 3)
(define winning-stars-number 11)
(define num-princess-spaces (- num-princess 1))
(define left-margin (/ (- (- window-width (* princess-space-size num-princess-spaces))
                          (* num-princess rectangle-picture-size)) 2))

(define-struct letter (character images) #:transparent)

(define alphabet (map char-upcase (string->list
                  "aábcčdďeěéfghiíjklmnopqrřsštťuúvwxyýzž")))

(define alphabet-short (map char-upcase (string->list
                  "abcdefghijklmnopqrstuvwxyz")))

(define answer-list images)
;; (define pict-alpha-duo (map (lambda (pict char) (cons pict char)) alphabet alphabet)) 

(define (select-randomly lst)
      (list-ref lst (random (length lst))))

;; game defines
(define-struct graphics (x y size img selected) #:transparent) ;; x y is top left
(define-struct answer (char graphics) #:transparent)
(define-struct start-screen (intro-text graphics) #:transparent)
(define-struct game-state (char-to-guess score answers) #:transparent)
(define-struct win-state (stars) #:transparent)

(define left-margin-selection (/ (- window-width (* 3 rectangle-picture-size)) 2))

(define (change-score fun gs)
  (make-game-state (game-state-char-to-guess gs)
                   (fun (game-state-score gs))
                   (game-state-answers gs)))

(define (increase-score gs x y)
  (let [(ngs (initialize-new-guess (add1 (game-state-score gs)) x y))]
    (if (> (game-state-score ngs) 5)
        (make-win-state (spawn-all-win-stars))
        ngs)))

(define (spawn-all-win-stars)
  (spawn-stars-in-rectangle window-width window-height))

(define (spawn-stars-in-rectangle width height)
  (let ([w-margin (inexact->exact (* 0.15 width))]
        [h-margin (inexact->exact (* 0.15 height))])
    (spawn-stars (random w-margin (- width w-margin))
                 (random h-margin (- height h-margin)))))

(define (spawn-stars x y)
  (let ([color-list (random-ref all-colors)])
    (for/list ([i winning-stars-number])
      (randomize-winstar x y color-list))))

(define (decrease-score gs)
  (change-score (lambda (x)(max 0 (sub1 x))) gs))


;; big bang callbacks

(define (select-uniq-random n lst)
  (define (accumulate accum)
    (cond
      [(or (= (length accum) n)
           (= (length accum) (length lst))) accum]
      [else
       (define item (select-randomly lst))
       (define already-there (member item accum))
       (if (boolean? already-there)   
           (accumulate (cons item accum))
           (accumulate accum))]
      ))
  (accumulate '()))

(define (generate-princess num left-x)
  (define random-princesses (select-uniq-random num answer-list))
  (define answer-total-width (+ rectangle-picture-size princess-space-size))
  (define left-xs (build-list num 
                              (lambda (x) 
                                (+ (* x answer-total-width)
                                   (+ left-x (/ rectangle-picture-size 2))))))
  (map (lambda (princess-tuple left-x)
         (make-answer (princess-char princess-tuple)
                      (make-graphics left-x (- (- window-height left-margin)
                                               (/ rectangle-picture-size 2))
                                     rectangle-picture-size (princess-image princess-tuple) #f)))
       random-princesses
       left-xs))

(define (draw-intro s)
  (place-image (text (start-screen-intro-text s) 48 "indigo")
               (/ window-width 2)
               50
               (rectangle window-width window-height "solid" "white")))

(define (select-color sel)
  (if sel "red" "black"))

(define (draw-princess  answers img)
  (foldl (lambda (answer img)
           (define g (answer-graphics answer))
           (place-image
            (underlay 
             (rectangle (graphics-size g)
                        (graphics-size g)
                        "solid"
                        (select-color (graphics-selected g)))
             (graphics-img g))
            (graphics-x g)
            (graphics-y g)
            img))
         img
         answers))

(define (draw-score score img)
  (define empty (empty-scene 0 20))
  (define colors '("white" "black" "blue" "green" "red" "hotpink"))
  (define star-img (star (* 10 score) "solid" (list-ref colors score)))
  (define (place-img img count)
    (cond
      [(eq? count 0) img]
      [else (place-img (beside star-img img) (sub1 count))]))
  (place-image
   star-img
   600
   45
   img))

(define (draw-game s)
  (begin
    ;;(define graphics (answer-graphics (game-state-answers s)))
    (draw-score (game-state-score s)
                (place-image (text (string (game-state-char-to-guess s)) 64 "HotPink")
                             (/ window-width 2)
                             150
                             (draw-princess (game-state-answers s) 
                                            (rectangle window-width window-height "solid" "white"))))))

(define (center-star sr)
  (let* ([w (image-width sr)]
         [h (image-height sr)]
         [d (max w h)]
         [blank  (circle d "solid" (color 255 255 255 0))])
    (place-image/align sr (- d (/ w 2)) (- d (/ h 2)) "left" "top" blank)))

(define (draw-win ws)  
    (foldl (lambda (cur-star img)
             (place-image
              (rotate (winstar-rot cur-star) (center-star (star (winstar-side cur-star) "solid" (winstar-color cur-star))))
              (winstar-x cur-star)
              (winstar-y cur-star)
              img))
           (rectangle window-width window-height "solid" "pink")
           (win-state-stars ws)))  

(define (draw state)
  (cond
    [(start-screen? state) (draw-intro state)]
    [(game-state? state) (draw-game state)]
    [(win-state? state) (draw-win state)]
    [else (error 'draw "unknown state")]))

(define (check-collision x graphics-center size)
  (and (> x (- graphics-center (/ size 2)))
       (< x (+ graphics-center (/ size 2)))))

(define (handle-key-event ws kevent)
  ws)

(define (update-winstate ws)
  (let* ([nws (make-win-state (filter (lambda (star)
                                      (let ([alpha (color-alpha (winstar-color star))])
                                        (> alpha 0)))
                                    (map (lambda (cur-star) (make-winstar (winstar-side cur-star)
                                                                          (struct-copy color (winstar-color cur-star)
                                                                             [alpha (max 0
                                                                                         (- (color-alpha (winstar-color cur-star))
                                                                                            (color-alpha (winstar-dcolor cur-star))))])
                                                                          (+ (winstar-x cur-star) (* 1 (winstar-dx cur-star)))
                                                                          (+ (winstar-y cur-star) (* 1 (winstar-dy cur-star)))
                                                                          (+ (winstar-rot cur-star) (winstar-drot cur-star))
                                                                          (winstar-dside cur-star)
                                                                          (winstar-dcolor cur-star)
                                                                          (winstar-dx cur-star)
                                                                          (winstar-dy cur-star)
                                                                          (winstar-drot cur-star)))
                                         (win-state-stars ws))))]
         [number-of-stars (length (win-state-stars nws))])
    (if (> number-of-stars 3)
        nws
        (make-win-state (append (win-state-stars nws) (spawn-all-win-stars))))))

(define (handle-tick-event ws) ;; this ticks 28fps
  (cond
    [(win-state? ws) (update-winstate ws)]
    [else ws]))

(define (initialize-new-guess score x y)
  (define answers (generate-princess num-princess left-margin))
  (define char-to-quess (answer-char (list-ref answers (random num-princess))))
  (make-game-state char-to-quess score ( mark-selected-answers answers x y)))

(define (handle-mouse-event-start-screen ws x y mevent)
  (cond
    [(equal? mevent "button-down") (initialize-new-guess 0 x y)]
    [else ws]))

(define (check-in-game-press ws x y mevent)
  (define selected
    (findf (lambda (answ)
             (graphics-selected (answer-graphics answ)))
           (game-state-answers ws)))
  (cond
    [(and selected (equal? (game-state-char-to-guess ws) (answer-char selected))) (increase-score ws x y)]
    [(and selected (not (equal? (game-state-char-to-guess ws) (answer-char selected)))) (decrease-score ws)]
    [else ws]))

(define (mark-selected-answers answers x y)
  (map (lambda (answer)
         (let ([graphics (answer-graphics answer)])
           (make-answer (answer-char answer)
                        (make-graphics (graphics-x graphics)
                                       (graphics-y graphics)
                                       (graphics-size graphics)
                                       (graphics-img graphics)
                                       (and (check-collision x (graphics-x graphics) (graphics-size graphics))
                                            (check-collision y (graphics-y graphics) (graphics-size graphics)))))))
       answers))

(define (copy-game-state ws x y)
  (make-game-state (game-state-char-to-guess ws)
                   (game-state-score ws)
                   (mark-selected-answers (game-state-answers ws) x y)))

(define (handle-mouse-event-game-state ws x y mevent)
  (cond
    [(equal? mevent "move") (copy-game-state ws x y)]
    [(equal? mevent "button-down") (check-in-game-press ws x y mevent)]
    [else ws]))

(define (handle-mouse-event-win-state ws x y mevent)
  (cond
    [(equal? mevent "button-down") default-start-screen]
    [else ws]
  ))

(define (handle-mouse-event ws x y mevent)
  (cond
    [(start-screen? ws) (handle-mouse-event-start-screen ws x y mevent)]
    [(game-state? ws) (handle-mouse-event-game-state ws x y mevent)]
    [(win-state? ws) (handle-mouse-event-win-state ws x y mevent)]
    [else ws]))

(define default-start-screen (make-start-screen "Ahoj Jůlinko" (list)))

;; Game Loop

(big-bang default-start-screen
          [to-draw draw]
          [on-key handle-key-event]
          [on-tick handle-tick-event]
          [on-mouse handle-mouse-event]
          [name "Písmenka"])